const express = require('express');
const router = express.Router();
const argon2 = require('argon2');
const { QueryTypes } = require('sequelize');  // :: เพื่อใช้แบบ query statement
const jwt = require('jsonwebtoken');

const model = require('../models/index');
const passportJWT = require('../middlewares/passport-jwt');

// get profile (คนที่กำลัง login อยู่)
/* /api/v1/users/profile */
router.get('/profile', [passportJWT.isLogin], async function(req, res, next) {
  // check login => [passportJWT.isLogin]

  const user = await model.User.findByPk(req.user.user_id);
  return res.status(200).json({
    user: {
      id: user.id,
      fullname: user.fullname,
      email: user.email,
      create_at: user.create_at
    }
  });
});

/* /api/v1/users */
router.get('/', async function(req, res, next) {

  // :: แบบ Model
  // const users = await model.User.findAll({
  //   // attributes: ['id', 'fullname']     //  เอาเฉพาะที่ต้องการ
  //   attributes: { exclude: ['password'] }, //  เอาทั้งหมด ยกเว้นเฉพาะที่ต้องการ
  //   order: [['id','desc']]
  // });

  // :: แบบ query statement
  const sql = 'SELECT id, fullname, email, created_at, updated_at FROM `users` order by id desc';
  const users = await model.User.sequelize.query(sql, {
    type: QueryTypes.SELECT
  });

  const totalUsers = await model.User.count();

  return res.status(200).json({
    message: 'all users',
    total: totalUsers,
    data: users
  });
});


/* /api/v1/users/register */
router.post('/register', async function(req, res, next) {
  const {fullname, email, password} = req.body;

  // 1. check duplicate email
  const user = await model.User.findOne({ where: { email: email } }); //{where: {db column: value}}
  if (user !== null) {
    return res.status(400).json({message: 'มีผู้ใช้งาน email นี้แล้ว'});
  }
  
  // 2. hash password
  const passHash = await argon2.hash(password);

  // 3. record to table
  const newUser = await model.User.create({
    fullname: fullname,
    email: email,
    password: passHash
  });


  return res.status(201).json({
    message: 'ลงทะเบียนสำเร็จ',
    user: {
      id: newUser.id,
      fullname: newUser.fullname
    }
  });
});


/* /api/v1/users/login */
router.post('/login', async function(req, res, next) {
  const {login, password} = req.body;

  // 1. check email
  const user = await model.User.findOne({ where: { email: login } }); //{where: {db column: value}}
  if (user === null) {
    return res.status(404).json({message: 'ไม่พบบัญชีผู้ใช้'});
  }

  // 2. check password login
  const isValid = await argon2.verify(user.password, password);
  if (!isValid) {
    return res.status(401).json({message: 'บัญชีผู้ใช้ หรือ รหัสผ่านไม่ถูกต้อง'});
  }

  // 3. Create token
  const token = jwt.sign({ user_id: user.id }, process.env.JWT, { expiresIn: '7d' }); // sign( playload, secretkey, option expire)

  return res.status(200).json({
    message: 'เข้าสู่ระบบสำเร็จ',
    access_token: token
    // ,user: {
    //   id: user.id,
    //   fullname: user.fullname,
    //   email: user.email
    // }
  });
});

module.exports = router;
